<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('message/list/{channelId}',['uses'=>'ChatChannelController@list','as'=>'']);
Route::post('message/send',['uses'=>'ChatChannelController@send','as'=>'']);


Route::post('channel/create',['uses'=>'ChatChannelController@create','as'=>'']);
Route::post('channel/seed',['uses'=>'ChatChannelController@seed','as'=>'']);