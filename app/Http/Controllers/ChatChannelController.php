<?php

namespace App\Http\Controllers;

use App\Event\MyEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use stdClass;

class ChatChannelController extends Controller
{
    public function create(Request $request)
    {
        $channelId = Uuid::uuid4()->toString();
        DB::table("channel_chat")->insert([
            'channel_id' => $channelId,
            'customer_id' => $request->get('customerId'),
            'partner_id' => $request->get('partnerId'),
            'status' => 'active',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        return ['channelId' => $channelId];
    }

    public function send(Request $request)
    {
        //create message in database
        $message = json_encode([
            'text' => $request->get('text'),
            'data' => new stdClass(),
            'image' => '',
            'location' => new stdClass(),
            'topic' => "simple text"
        ]);
        DB::table("channel_chat_messages")->insert(
            [
                'message_id' => Uuid::uuid4(),
                'channel_id' => $request->get('channelId'),
                'sender_id' => $request->get('senderId'),
                'message' => $message,
                'version' => "0.1.0",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        //fire event to pusher
        event(new MyEvent($message, $request->get('channelId'), 'new message'));

        return 'sent';
    }

    public function list($channelId)
    {
        $chatMessages = DB::table('channel_chat_messages')->where(["deleted_at" => null, 'channel_id' => $channelId])
            ->orderBy('created_at', 'desc')->take(50)->get();

        $response = [];

        foreach ($chatMessages as $chatMessage) {
            $response[] = [
                'messageId' => $chatMessage->message_id,
                'sentAt' => $chatMessage->created_at,
                'message' => json_decode($chatMessage->message),
                'sender' => ['id' => $chatMessage->sender_id]
            ];
        }

        return response($response);
    }


    public function seed(Request $request)
    {
        $channelId = $request->get('channelId');
        $customerId = $request->get('customerId');
        $partnerId = $request->get('partnerId');
        $seeder = new \FakeChatMessage($channelId,$customerId,$partnerId);
        $seeder->run();
        return ['channelId'=>$seeder->channelId];
    }
}
