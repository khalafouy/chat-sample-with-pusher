<?php


namespace App\Event;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MyEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $channelId;
    public $eventName;

    /**
     * MyEvent constructor.
     * @param $message
     * @param $channelId
     * @param $eventName
     */
    public function __construct($message, $channelId, $eventName)
    {
        $this->message = $message;
        $this->channelId = $channelId;
        $this->eventName = $eventName;
    }


    public function broadcastOn()
    {
        return [$this->channelId];
    }

    public function broadcastAs()
    {
        return $this->eventName;
    }
}