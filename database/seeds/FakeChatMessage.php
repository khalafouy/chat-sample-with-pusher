<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class FakeChatMessage extends Seeder
{
    public $channelId;
    public $customerId;
    public $partnerId;

    /**
     * FakeChatMessage constructor.
     * @param null $channelId
     * @param null $customerId
     * @param null $partnerId
     */
    public function __construct($channelId = null, $customerId = null, $partnerId = null)
    {
        $this->channelId = $this->channelId ?: Uuid::uuid4()->toString();
        $this->partnerId = $this->partnerId ?: 2;
        $this->customerId = $this->customerId ?: 1;

    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('channel_chat')->truncate();
        DB::table('channel_chat_messages')->truncate();
        $faker = Faker\Factory::create('en_US');
        $now = Carbon::now();

        DB::table("channel_chat")->insert([
            'channel_id' => $this->channelId,
            'customer_id' => $this->customerId,
            'partner_id' => $this->partnerId,
            'status' => 'active',
            'created_at' => $now,
            'updated_at' => $now
        ]);


        for ($i = 0; $i < 50; $i++) {
            $messageSentTime = $now->subSeconds(rand(20, 120));
            $message = json_encode([
                'text' => $faker->text(),
                'data' => new stdClass(),
                'image' => '',
                'location' => new stdClass(),
                'topic' => "simple text"
            ]);
            DB::table("channel_chat_messages")->insert(
                [
                    'message_id' => Uuid::uuid4(),
                    'channel_id' => $this->channelId,
                    'sender_id' => rand(1, 2),
                    'message' => $message,
                    'version' => "0.1.0",
                    'created_at' => $messageSentTime,
                    'updated_at' => $messageSentTime
                ]);

            //fire event to pusher
            //event(new MyEvent($message, $request->get('channelId'), 'new message'));

        }
    }
}
